#include "inverter.h"

#include <functional>
#include "interval_halver.h"
#include "prefixes.h"

double Inverter::VOUT() const
{
	std::function<double(double)> fn = [&](double VOUT)
	{
		// Az nMOS tranzisztor esetén
		double VGSN = _VIN;
		double VDSN = VOUT;
		return _nmos.ID(VGSN, VDSN);
	};

	std::function<double(double)> fp = [&](double VOUT)
	{
		// A pMOS tranzisztorra pedig
		double VGSP = _VIN - _VDD;
		double VDSP = VOUT - _VDD;
		return _pmos.ID(VGSP, VDSP);
	};

	std::function<double(double)> f = [&fn, &fp](double VOUT)
	{
		return fn(VOUT) - fp(VOUT);
	};

	double VOUT = IntervalHalver::solve(-_VDD, 2.0*_VDD, f, mikro);
	return VOUT;
}
