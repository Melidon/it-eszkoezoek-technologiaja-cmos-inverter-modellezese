#include <iostream>
#include "nmos.h"
#include "pmos.h"
#include "inverter.h"
#include "prefixes.h"

int main(void)
{
	Nmos nmos{120.0 * mikro, 0.8, 1.0 * mikro, 1.0 * mikro};
	Pmos pmos{40.0 * mikro, -0.8, 1.0 * mikro, 3.0 * mikro};
	double VDD = 3.0;
	Inverter inverter{nmos, pmos, VDD};
	for (double d = 0.0; d <= 3.0; d += 0.01)
	{
		inverter.VIN() = d;
		std::cout << d << "," << inverter.VOUT() << std::endl;
	}
	return 0;
}
