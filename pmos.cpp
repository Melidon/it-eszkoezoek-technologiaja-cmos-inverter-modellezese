#include "pmos.h"

double Pmos::ID(const double VGS, const double VDS) const
{
	double ID = 0.0;
	if (VGS > _VT)
	{
		ID = 0.0; // azaz a tranzisztor nem vezet
	}
	if (VGS < _VT) // a tranzisztor vezet
	{
		if (VDS > VGS - _VT) // azaz lineárs tartomanyban
		{
			ID = (_K / 2.0) * (_W / _L) * (2.0 * (VGS - _VT) * VDS - VDS * VDS);
		}
		else // egyébként pedig (telítésben)
		{
			ID = (_K / 2.0) * (_W / _L) * (VGS - _VT) * (VGS - _VT);
		}
	}
	return ID;
}
