#pragma once

#include <functional>

class IntervalHalver
{
public:
	static double solve(const double x1, const double x2, std::function<double(double)> &f, const double epsilon);
};
