#include "prefixes.h"

#include <cmath>

const double pico = pow(10.0, -12.0);
const double nano = pow(10.0, -9.0);
const double mikro = pow(10.0, -6.0);
const double milli = pow(10.0, -3.0);
