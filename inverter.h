#pragma once

#include "nmos.h"
#include "pmos.h"

class Inverter
{
private:
	const Nmos _nmos;
	const Pmos _pmos;
	double _VDD;
	double _VIN;

public:
	Inverter(const Nmos nmos, const Pmos pmos, const double VDD = 5, const double VIN = 0)
		: _nmos{nmos},
		  _pmos{pmos},
		  _VDD{VDD},
		  _VIN{VIN} {}

	const Nmos &nmos() const { return _nmos; }
	const Pmos &pmos() const { return _pmos; }

	double &VDD() { return _VDD; }
	const double &VDD() const { return _VDD; }
	double &VIN() { return _VIN; }
	const double &VIN() const { return _VIN; }

	double VOUT() const;
};
