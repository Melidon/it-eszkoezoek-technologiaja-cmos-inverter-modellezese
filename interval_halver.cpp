#include "interval_halver.h"

#include <stdexcept>
#include <algorithm>

double IntervalHalver::solve(const double x1, const double x2, std::function<double(double)> &f, const double epsilon)
{
	if (!(f(x1) * f(x2) < 0.0))
	{
		throw std::invalid_argument("IntervalHalver::solve:: \"f(x1) * f(x2) < 0\" must be true");
	}

	double start = std::min(x1, x2);
	double end = std::max(x1, x2);
	double middle = (start + end) / 2.0;

	while (end - start > epsilon)
	{
		if (f(middle) * f(start) < 0.0)
		{
			end = middle;
		}
		else
		{
			start = middle;
		}
		middle = (start + end) / 2.0;
	}

	return middle;
}
