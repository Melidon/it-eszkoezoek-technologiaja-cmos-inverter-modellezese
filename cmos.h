#pragma once

class Cmos
{
protected:
	const double _K;  // áramkonstans
	const double _VT; // küszöbfeszültség
	const double _L;  // a tranzisztor csatornájának hosszúsága
	const double _W;  // a tranzisztor csatornájának szélessége
public:
	Cmos(const double K, const double VT, const double L, const double W) : _K{K}, _VT{VT}, _L{L}, _W{W} {}
	
	const double &K() const { return _K; }
	const double &VT() const { return _VT; }
	const double &L() const { return _L; }
	const double &W() const { return _W; }

	virtual double ID(const double VGS, const double VDS) const = 0;
};
