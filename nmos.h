#pragma once

#include "cmos.h"

class Nmos : public Cmos
{
public:
	Nmos(const double K, const double VT, const double L, const double W) : Cmos{K, VT, L, W} {}

	double ID(const double VGS, const double VDS) const override;
};
