#pragma once

#include "cmos.h"

class Pmos : public Cmos
{
public:
	Pmos(const double K, const double VT, const double L, const double W) : Cmos{K, VT, L, W} {}

	double ID(const double VGS, const double VDS) const override;
};
